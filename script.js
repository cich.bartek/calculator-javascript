// console.log("Hello")

let display = document.getElementById('displayblock');

let btn = Array.from(document.getElementsByClassName('button'));

// console.log(btn);

btn.map(button => {
    button.addEventListener('click', (a) => {
        switch(a.target.innerText){
            case 'C':
                display.innerText = '';
                break;
            case '=':
                try{
                    display.innerText = eval(display.innerText);
                } catch {
                    display.innerText = "Błąd"
                }
                break;
            case '←':
                if (display.innerText){
                   display.innerText = display.innerText.slice(0, -1);
                }
                break;
            default:
                display.innerText += a.target.innerText;
        }
    });
});